﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework 
{
    

    public class SampleEntity : MonoBehaviour
    {
        //Name for a Pet
        private string name = "Napfkuchen";
        public string Name { get => name; set => name = value; }

        //Name for a Breed
        private string breed = "Chonk";

        public string Breed
        {
             get {return breed;}
             set {breed = value;}

        }

        //Age of the Pet
        private int age = 5;

        public int Age
        {
            get {return age;}
            set {age = value;}
        }

        //Weight of the Chonker
        private float weight = 15.5f;
        
        public float Weight
        {
            get {return weight;}
            set {weight = value;}
        }

        //Pets alignment
        private bool goodBoi = true;

        public bool GoodBoi
        {
            get {return goodBoi;}
            set {goodBoi = value;}
        }

        


        //pets make noise barkbarkwoof
        void MakeNoise()
        {
    
        }

       //pets can leave little presents, get your bags ready
        void MakePoop(float amount)
        {
        
        }
    }
}

