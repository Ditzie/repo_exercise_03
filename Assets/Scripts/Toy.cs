﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Toy : MonoBehaviour
    {
        // Every Pet needs a Toy 
        private string name = "Squeaky Duck";

        public string Name
        {
            get { return name; }
        }

        //Toys have a price from 1-100
        private float price = 20.5f;

        public float Price
        {
            get { return price; }
        }

        private int quality = 10;

        
        //Toys have a quality rating from 1-10
        public int Quality 
        {
            set {quality = value;}
        }

        //Toys can break after time
        private bool broken = false;

        public bool Broken
        {
            set {broken = value;}
        }
    }

}
