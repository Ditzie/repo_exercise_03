﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{


    public class Person : MonoBehaviour
    {
        //made thingy public,assigned in unity to SampleEntity (Gameobject)
        public SampleEntity thingy;

        void Start()
        {
            string name = thingy.Name;
            string breed = thingy.Breed;
            int age = thingy.Age;
            float weight = thingy.Weight;
            bool goodBoi = thingy.GoodBoi;

            Debug.Log(name);
            Debug.Log(breed);
            Debug.Log(age);
            Debug.Log(weight);
            Debug.Log(goodBoi);

            goodBoi = false;
            Debug.Log(goodBoi);
        }
    }


}
